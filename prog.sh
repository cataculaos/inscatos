#! /bin/bash
sudo pacman -S bspwm sxhkd polybar nitrogen rofi dunst picom fish
sudo pacman -S telegram-desktop
sudo pacman -S keepassxc thunderbird audacity atril android-tools krita inkscape obs-studio qbittorrent xclip vlc lm_sensors flameshot gimp syncthing obsidian libreoffice firefox ranger keepassxc kdeconnect discord htop btop wireguard-tools
sudo pacman -S nerd-fonts
sudo pacman -S papirus-icon-theme
sudo pacman -S python3 git python-matplotlib python-numpy python-scipy
sudo pacman -S powertop p7zip unrar-free brightnessctl
sudo pacman -S neofetch
mkdir ~/Aur
cd ~/Aur/
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
paru -i spotify
paru -i vscodium
