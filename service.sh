cd ~/.config/
mkdir ~/.icons
mkdir ~/.themes
git clone git@gitlab.com:cataculaos/dunst.git
git clone git@gitlab.com:cataculaos/gtk_dots.git
cp -rf /gtk_dots/* ./
git clone git@gitlab.com:cataculaos/bspwm.git
git clone git@gitlab.com:cataculaos/alacritty.git
git clone git@gitlab.com:cataculaos/picom.git
git clone git@gitlab.com:cataculaos/polybar.git
git clone git@gitlab.com:cataculaos/ranger.git
git clone git@gitlab.com:cataculaos/rofi.git
git clone git@gitlab.com:cataculaos/sxhkd.git
git clone git@gitlab.com:cataculaos/xterm.git
cp xterm/.Xresources ~/
git clone git@gitlab.com:cataculaos/sddm.git
sudo cp -rf sddm/* /usr/share/sddm/themes/
chsh -s $(which fish)
sudo systemctl enable syncthing@qapfuc
sudo systemctl start syncthing@qapfuc
git config --global user.email "qapfuc@gmail.com"
git config --global user.name "qapfuc"
cd ~/.icons
mkdir Catacula 
git clone git@gitlab.com:cataculaos/icons.git
cp -rf icons/ ./Catacula
cd ~/.themes
mkdir Catacula
git clone git@gitlab.com:cataculaos/gtk.git
cp -rf gtk/ ./Catacula
sudo nvim /etc/sddm.conf